const express = require('express');
const mongodb = require('mongodb');
const cors = require('cors');
const randomstring = require('randomstring');

const PORT = 5000;

const app = express();
app.use(express.json());
app.use(express.static(__dirname));
app.use(cors({optionsSuccessStatus: 200})); // Use cors middleware to handle CORS headers

let database;

app.post('/', async (req, res) => {
    try {
        const { url } = req.body;
        const short = randomstring.generate(7);
        const obj = { url, short };
        await database.collection('urls').insertOne(obj);
        res.json({msg: obj});
    } catch (err) {
        res.send(err.message);
    }
});

app.get('/:short', async (req, res) => {
    try {
        const short = req.params.short;
        const result = await database.collection('urls').findOne({ short });
        res.redirect(result.url);
    } catch (err) {
        res.send(err.message);
    }
});

async function connectDB() {
    const mongoClient = mongodb.MongoClient;
    const db = await mongoClient.connect('mongodb://db:27017');
    database = db.db('url-shortner');
    database.createCollection('urls');
}

app.listen(PORT, async () => {
    await connectDB();
    console.log(`listening on port ${PORT}`);
});
