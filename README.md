## Installation
```
git clone https://gitlab.com/xccelerator/link-shortener.git
cd link-shortener
npm install
```

## Start project
```
1. Run mongodb database
2. Change connection string in `index.js`
3. Open cmd with directory of project
4. npm run
```

## Start docker compose
```
1. Open cmd with directory of project
2. docker-compose up
```

## Demo
#### Body for request to API
```json
{
    "url": "http://google.com"
}
```
![banner](/img/req.png)

Know you can visit site `localhost:5000/zU75IBM` and will be redirected to google.com